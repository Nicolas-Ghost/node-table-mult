const fs = require("fs");
const colors = require('colors/safe')

const crearArchivo = async ( base, l, h) => {
  try {
    
    let out, consola = "";
    

    for (let i = 1; i <= h; i++) {
      out += `${base} x ${i} = ${base * i}\n`;
      consola += `${colors.gray(base)} x ${colors.blue(i)} = ${colors.cyan(base * i)}\n`;
    }
    
    if (l != false) {
      console.log(colors.yellow("========================="));
      console.log(`      ${colors.green('TABLA DEL ', base) }`) ;
      console.log(colors.yellow("========================="));
      console.log(consola);
    }


    fs.writeFileSync(`./out/tabla-${base}.txt`, out);

    return `tabla-${base}.txt`

  } catch (error) {
    throw error;
  }
}

module.exports = {
    crearArchivo
}
