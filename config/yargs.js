const argv = require('yargs')
    .option('b',{
        alias: 'base',
        type: 'number',
        describe: 'Es la base de la tabla de multiplicar',
        demandOption: true
    })
    .option('l', {
        alias: 'listar',
        type: 'boolean',
        describe: 'Muestra la tabla en consola',
        default: false,
    })
    .option('h', {
        alias: 'hasta',
        type: 'number',
        default: 10,
        describe: 'El limite de la tabla'
    })
    .check((argv, options) => {
        console.log('yargs:', argv);
        if (isNaN( argv.b )) {
            throw 'la base tiene que ser un num'
        } else {
            return true   
        }
    } )
    .argv;

    // Esto es un obj
module.exports = argv;