
const { crearArchivo } = require('./helpers/multiplicar');
const argv = require('./config/yargs')
const colors = require('colors/safe')


console.clear();

// const [,,arg3 = 'base=2'] = process.argv
// const [, base = 2] = arg3.split('=')
// console.log(base)

// console.log(process.argv)
// console.log(argv)

// const base = 10;

crearArchivo( argv.b, argv.l, argv.h )
    .then( nameArchivo => console.log( colors.rainbow(`${nameArchivo} nice`) ) )
    .catch( err => console.log(err) )